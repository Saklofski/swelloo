import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { onLogin } from '../store/actions/user.actions';
import { HomeFooter } from '../cmps/home-footer';
import { ReactComponent as FlatIcon } from '../assets/svg/flat-icon.svg';
import { ReactComponent as LoginLeft } from '../assets/svg/login-left.svg';
import { ReactComponent as LoginRight } from '../assets/svg/login-right.svg';

class _Login extends React.Component {
  state = {
    user: {
      username: '',
      password: '',
    },
    errMsg: '',
    isFromInvite: false
  };

  componentDidMount () {
    this.setState({ isFromInvite: this.props.match.path.startsWith('/invite') ? true : false });
  }

  handleChange = ({ target }) => {
    const field = target.name;
    const value = target.value;
    this.setState(prevState => ({ ...prevState, user: { ...prevState.user, [field]: value } }));
  };

  onLogin = async ev => {
    if (ev) ev.preventDefault();
    const { user, isFromInvite } = this.state;
    const { boardId } = this.props.match.params;
    console.log(user,isFromInvite);
    try {
      await this.props.onLogin(user, this.showErrorMsg);
      isFromInvite ? this.props.history.push(`/invite/${ boardId }`) : this.props.history.push('/board');
    } catch (err) {
      this.showErrorMsg(err);
    }
  };

  showErrorMsg = err => {
    this.setState({ errMsg: 'Invalid username/password' });
  };

  render () {
    const { user, errMsg, isFromInvite } = this.state;
    return (
      <section className="login">
        <div className="login-left-img">
          <LoginLeft />
        </div>
        <div className="login-right-img">
          <LoginRight />
        </div>
        <Link to="/" className="header-logo flex align-center justify-center">
          <FlatIcon />
          <span>Powerful</span>
        </Link>
        <form className="flex column align-center" onSubmit={this.onLogin}>
          {errMsg && (
            <div className="error">
              <p>{errMsg}</p>
            </div>
          )}
          <h1>{isFromInvite ? 'Please log in first to join the board' : 'Log in to Powerful'}</h1>
          <input
            autoCorrect="off"
            type="email"
            name="username"
            placeholder="Enter email"
            value={user.username}
            onChange={this.handleChange}
            required
          />
          <input
            autoCorrect="off"
            type="password"
            name="password"
            placeholder="Enter password"
            value={user.password}
            onChange={this.handleChange}
            autoComplete="suggesed-password"
            required
          />
          <button type="submit">Log in</button>
          <hr className="bottom-form-separator"></hr>
          <Link to={isFromInvite ? `/invite/${ this.props.match.params.boardId }/signup` : '/signup'}>New here? Sign up for an account</Link>
        </form>
        <HomeFooter />
      </section>
    );
  }
}

const mapDispatchToProps = {
  onLogin,
};

export const Login = connect(null, mapDispatchToProps)(_Login);
